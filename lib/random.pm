package random;

use strict;
use warnings;

use Data::Dumper;

sub createAction {
  my ($self, $name, $params ) = @_;

  return sub {
    my ($context) = @_;
    $context ||= {};
    $context->{records} = [1, 2, 3];

    return $context;
  };
}

1;
