package dummy;

use strict;
use warnings;

use Data::Dumper;
use Clone qw(clone);

sub createAction {
    my ($self, $name, $params ) = @_;

    if (ref($params->{records}) ne "ARRAY") {
        die "Error - $name: missing records";
    }

    return sub {
        my ($context) = @_;
        $context ||= {};

        if ($context->{verbose}) {
            print "dummy stage - $name running\n";
        }

        foreach my $recordSet (keys %{$params}) {
            $context->{recordSets}{$recordSet} = clone $params->{$recordSet};
        }

        return $context;
    };
}

1;
