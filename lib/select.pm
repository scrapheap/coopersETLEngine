package select;

use strict;
use warnings;

use Data::Dumper;
use Clone qw(clone);

sub createAction {
    my ($self, $name, $params ) = @_;

    if (!$params->{source}) {
        die "Error - $name: missing records";
    }

    if (!$params->{sql}) {
        die "Error - $name: missing sql parameter";
    }

    my $recordSet = $params->{recordSet} || 'records';

    return sub {
        my ($context) = @_;
        $context ||= {};

        if ($context->{verbose}) {
            print "select stage - $name running\n";
        }

        if (!$context->{sources}{$params->{source}}) {
            die "Missing source - $params->{source}";
        }

        my $dbh = $context->{sources}{$params->{source}}->database($params->{database} || ());
        my $select = $dbh->prepare($params->{sql});
        $select->execute();

        my @records;

        while (my $record = $select->fetchrow_hashref()) {
            push @records, $record;
        }

        $context->{recordSets}{$recordSet} = \@records;

        return $context;
    };
}

1;
