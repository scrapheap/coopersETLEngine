package dumper;

use strict;
use warnings;

use Data::Dumper;

sub createAction {
  my ($self, $name, $params ) = @_;

  return sub {
    my ($context) = @_;

    if ($context->{verbose}) {
        print "dumper stage - $name\n";
    }

    my $recordSet = $context->{recordSets}{$params->{recordSet} || 'records'};

    foreach my $record (@{$recordSet}) {
        print Dumper($record);
    }

    return $context;
  };
}

1;
