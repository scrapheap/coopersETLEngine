
package ETLEngine::Pipe;

use strict;
use warnings;

use Object::InsideOut;
use YAML::XS;
use Data::Dumper;
use Try::Tiny;
use List::Util qw(reduce first);

use ETLEngine::System;

my @name :Field;
my @description :Field;
my @sources :Field;
my @targets :Field;
my @pipe :Field;
my @verbose :Field;
my @systemPath :Field;


my %init_args :InitArgs = (
    file => { default => '' },
    verbose => { default => 0 },
    systemPath => { default => ['systems'] },
);


sub _init :Init {
    my ($self, $args) = @_;

    if (exists($args->{verbose})) {
        $verbose[$$self] = $args->{verbose};
    }

    if (exists($args->{systemPath})) {
        $systemPath[$$self] = $args->{systemPath};
    }

    if (exists($args->{file})) {
        $self->loadPipe($args->{file})
    }
}

sub _destroy :Destroy {
    my ($self) = @_;

    # Close all connections to source systems
    foreach my $source (values %{$sources[$$self]}) {
        $source->close();
    }
}

sub loadPipe {
    my ($self, $file) = @_;

    my $pipeSrc = YAML::XS::LoadFile($file);

    $name[$$self] = $pipeSrc->{name} || '';
    $description[$$self] = $pipeSrc->{description} || '';

    %{$sources[$$self]} = map { $_ => $self->_loadSystem($_) } @{$pipeSrc->{sources}};
    %{$targets[$$self]} = map { $_ => $self->_loadSystem($_) } @{$pipeSrc->{targets}};
    
    $pipe[$$self] = [];

    foreach my $stage (@{$pipeSrc->{stages}}) {
        my $name = $stage->{name} || '';

        KEY:
        foreach my $action (keys %{$stage}) {
            next KEY if $action eq 'name';
            try {
                require "$action.pm";

                push @{$pipe[$$self]}, {
                    name => $name,
                    action => $action->createAction($name, $stage->{$action}),
                }
            } catch {
                die "$_";
            };
        }
    }

    return $self;
}

sub run {
    my ($self) = @_;

    my $context = {
        verbose => $verbose[$$self],
        sources => $sources[$$self],
        targets => $targets[$$self],
    };

    $context = reduce {$b->{action}->($a)} $context, @{$pipe[$$self]};

    return $self;
}

sub dump {
    my ($self) = @_;

    return Dumper($name[$$self]) .
        Dumper($description[$$self]) .
        Dumper($pipe[$$self]);
}

sub sources {
    my ($self) = @_;

    return keys %{$sources[$$self]};
}

sub targets {
    my ($self) = @_;

    return keys %{$targets[$$self]};
}

sub _loadSystem {
    my ($self, $system) = @_;

    my $path = first { -e "$_/$system.yml" } @{$systemPath[$$self]};

    if (!$path) {
        die "Unable to find '$system' config file";
    }

    return ETLEngine::System->new(file => "$path/$system.yml");
}

1;
