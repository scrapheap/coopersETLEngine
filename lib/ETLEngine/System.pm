
package ETLEngine::System;

use strict;
use warnings;

use Object::InsideOut;
use YAML::XS;
use Data::Dumper;
use Try::Tiny;
use DBI;


my @name :Field;
my @description :Field;
my @databaseHandles :Field;
my @databases :Field;
my @verbose :Field;

my %init_args :InitArgs = (
    file => '',
    verbose => 0,
    databaseHandles => {},
    databases => {},
);

sub _init :Init {
    my ($self, $args) = @_;

    if (exists($args->{verbose})) {
        $verbose[$$self] = $args->{verbose};
    }

    if (exists($args->{file})) {
        $self->loadSystem($args->{file})
    }
}

sub loadSystem {
    my ($self, $file) = @_;

    my $systemSrc = YAML::XS::LoadFile($file);

    $name[$$self] = $systemSrc->{name} || '';
    $description[$$self] = $systemSrc->{description} || '';
    
    foreach my $database (@{$systemSrc->{databases}}) {
        my $name = $database->{name} || 'default';
        $databases[$$self]->{$name} = $database;
    }

    return $self;
}

sub database {
    my ($self, $database) = @_;

    $database ||= 'default';

    if (!$databaseHandles[$$self]->{$database}) {

        my $driver = $databases[$$self]->{$database}->{type};
        my %validParameters = map { $_ => 1 } qw(database host port);

        my @parameters = map {
            $validParameters{$_} ? "$_=$databases[$$self]->{$database}->{$_}" : ()
        } keys %{$databases[$$self]->{$database}};

        $databaseHandles[$$self]->{$database} = DBI->connect(
            "dbi:$driver:" . join (';', @parameters),
            $databases[$$self]->{$database}->{username},
            $databases[$$self]->{$database}->{password},
            {
                PrintError => 0,
                RaiseError => 1,
                AutoCommit => 0,
            }
        );
    }

    return $databaseHandles[$$self]->{$database};
}

sub close {
    my ($self) = @_;

    foreach my $database (values %{$databaseHandles[$$self]}) {
        $database->disconnect();
    }

    return;
}

1;
