DROP DATABASE IF EXISTS ETL;
CREATE DATABASE ETL;
USE ETL;

CREATE TABLE people (
    id INT unsigned auto_increment primary key,
    name VARCHAR(255),
    age INT unsigned,
    lastModified TIMESTAMP
);

INSERT INTO people (name, age) VALUES
    ('bob', 20),
    ('fred', 30),
    ('sid', 25);
