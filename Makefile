
.PHONY: all
all:

.PHONY: test
test: unitTests

.PHONY: unitTests
unitTests: t/*.t
	mysql < sql/resetEtl.sql
	prove -I./lib -I./blib -It -It/lib $^
