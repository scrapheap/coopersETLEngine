#!/usr/bin/perl

use strict;
use warnings;

use Test::More;
use Test::Deep;
use Test::Exception;

use Data::Dumper;

use testStage;
use storeRecordSet;

plan tests => 5;
use_ok("ETLEngine::Pipe");

subtest 'Basic Pipe' => sub {
    plan tests => 2;

    my $pipe = ETLEngine::Pipe->new(file => 't/pipes/basic.yml');

    cmp_deeply(
        [testStage->calls()],
        ["first test stage", "second test stage"],
        "The stage actions should be prepared in the order specified"
    );

    $pipe->run();

    cmp_deeply(
        [testStage->actions()],
        ["first test stage", "second test stage"],
        "The stage actions should be called in the same order as they were specified in the pipe"
    );

    testStage->reset();
};


subtest 'Faulty Pipe' => sub {
    plan tests => 1;
    throws_ok { my $pipe = ETLEngine::Pipe->new(file => 't/pipes/faulty.yml') } qr/Error: Can't locate unknownStage.pm/, "an unknown stage should fail";
};


subtest 'Sources Pipe' => sub {
    plan tests => 2;

    my $pipe = ETLEngine::Pipe->new(file => 't/pipes/sources.yml', systemPath => ['t/systems']);

    cmp_deeply([$pipe->sources()], ['localhost'], 'The sources listed should be loaded');

    $pipe->run();

    cmp_deeply(
        storeRecordSet->recordSet(),
        [
            {'name' => 'Fred Blogs', 'age' => 20},
            {'age' => 25, 'name' => 'Sid Blogs'},
        ],
        "The data source should return the expected records"
    );

    storeRecordSet->reset();
};

subtest 'Targets Pipe' => sub {
    plan tests => 1;

    my $pipe = ETLEngine::Pipe->new(file => 't/pipes/targets.yml', systemPath => ['t/systems']);

    cmp_deeply([$pipe->targets()], ['localhost'], 'The targets listed should be loaded');
};
