#!/usr/bin/perl

use strict;
use warnings;

use Test::More;
use Test::Deep;
use Test::Exception;

use Data::Dumper;

use ETLEngine::Pipe;

use storeRecordSet;

plan tests => 1;

subtest 'Mysql Sources Pipe' => sub {
    plan tests => 1;

    my $pipe = ETLEngine::Pipe->new(file => 't/pipes/mysql.yml', systemPath => ['t/systems']);

    $pipe->run();

    cmp_deeply(
        storeRecordSet->recordSet(),
        [
            {name => 'bob', age => 20},
            {name => 'fred', age => 30},
            {name => 'sid', age => 25},
        ],
        "The data source should return the expected records"
    );

    storeRecordSet->reset();
};
