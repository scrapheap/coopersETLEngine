#!/usr/bin/perl

use strict;
use warnings;

use Test::More;
use Test::Deep;
use Test::Exception;

use Data::Dumper;

plan tests => 2;

use_ok("ETLEngine::System");

my $system = ETLEngine::System->new(file => 't/systems/localhost.yml');

is (ref $system->database(), 'DBI::db', 'The connection to the default database should be a standard DBI connection');

