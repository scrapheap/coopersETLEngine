#!/usr/bin/perl

use strict;
use warnings;

use Test::More;
use Test::Deep;

plan tests => 3;

require_ok("dumper.pm");

my $params = {
    allFields => 1,
};

my $action = dumper->createAction("Test Action", $params);

is (ref($action), "CODE", "createAction should return a sub we can call");

cmp_deeply(
    $action->({recordSets => {records => [{a => "1", b => "2"}]}}),
    {
        recordSets => {
            records =>  [{a => "1", b => "2"}],
        },
    },
    "When called the action is called the records shouldn't change"
);
