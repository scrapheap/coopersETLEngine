package storeRecordSet;

use strict;
use warnings;

use Data::Dumper;

my $storedRecordSet = [];

sub createAction {
    my ($self, $name, $params ) = @_;

    my $recordSet = $params->{recordSet} || 'records';

    return sub {
        my ($context) = @_;
        $context ||= {};

        $storedRecordSet = $context->{recordSets}{$recordSet};

        return $context;
    };
}

sub recordSet {
    return $storedRecordSet;
}

sub reset {
    $storedRecordSet = [];

    return;
}

1;
