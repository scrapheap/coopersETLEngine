package testStage;

use strict;
use warnings;

use Data::Dumper;

my @testStageCalls;
my @testStageActions;

sub createAction {
    my ($self, $name, $params ) = @_;

    push @testStageCalls, $name;

    return sub {
        my ($context) = @_;
        $context ||= {};

        push @testStageActions, $name;

        return $context;
    };
}

sub calls {
    return @testStageCalls;
}

sub actions {
    return @testStageActions;
}

sub reset {
    @testStageCalls = ();
    @testStageActions = ();

    return;
}

1;
