#!/usr/bin/perl

use strict;
use warnings;

use Test::More;
use Test::Deep;

plan tests => 3;

require_ok("dummy.pm");

my $params = {
    records => [
        {
            name => "fred",
            age => "10",
        }, {
            name => "freda",
            age => "11",
        },
    ],
};

my $action = dummy->createAction("Test Action", $params);

is (ref($action), "CODE", "createAction should return a sub we can call");

cmp_deeply(
    $action->({}),
    {
        recordSets => {
            records=>[
                { name => "fred", age => "10" },
                { name => "freda", age => "11" },
            ],
        },
    },
    "When called the action should set up the records as expected"
);
