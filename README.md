# Cooper's ETL Engine

A simple ETL engine that lets you define a set of steps to be performed.

## Getting started

If you have Vagrant set up on your machine then you can have a play with the engine
by cloning the repository and then running 

```
vagrant up
```

followed by 

```
vagrant ssh
```

once connected you'll need to change to the `/vagrant` directory

```
cd /vagrant
```

once there you can run the etlEngine wit the test pipe with the command

```
etlEngine --verbose pipes/test.yml
```

## Pipes

Pipes are YAML files and look like the following:

```
---
name: Test pipe

description: A test pipe

sources:
  - random

targets:
  - dumper

stages:
  - name: create random records
    dummy:
        records:
          - name: Fred Blogs
            age: 20
          - name: Sid Blogs
            age: 25

  - name: record dump
    dumper:
        allFields: true
```

## Testing

To run the unit test simply `vagrant up` a machine `vagrant ssh` into it, `cd /vagrant`
then `make test`.

